# README

## Important:

Before running the application, put your secret and public **Marvel API keys** in the constants class:

`src/main/java/br/com/treinamento/dojo/utils/Constants.java`

Ideally, these keys should be in a external configuration file or in the database.

## Documentation

Once the application is running, go to http://localhost:8080/swagger-ui.html to view the documentation and execute test calls.

The POST - marvel/characters/load method loads into memory some data and a list of comics from all characters whose name starts with the given "name" parameter, which is required.

The other methods execute operations on the loaded or created data.


