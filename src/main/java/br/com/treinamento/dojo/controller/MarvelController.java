package br.com.treinamento.dojo.controller;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.entities.ApiMarvelResponse;
import br.com.treinamento.dojo.entities.Character;
import br.com.treinamento.dojo.entities.Comic;
import br.com.treinamento.dojo.entities.LoadResponse;
import br.com.treinamento.dojo.entities.requests.AlterDescriptionRequest;
import br.com.treinamento.dojo.entities.requests.CreateCharacterRequest;
import br.com.treinamento.dojo.entities.requests.CreateComicRequest;
import br.com.treinamento.dojo.exception.MissingFieldsException;
import br.com.treinamento.dojo.services.MarvelPool;
import br.com.treinamento.dojo.services.MarvelServices;

@RestController
@RequestMapping(value = "/marvel")
public class MarvelController {

	@Autowired
	private MarvelPool marvelPool;

	@Autowired
	private MarvelServices marvelServices;
	
	private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

	@RequestMapping(value = "/characters/load", method = RequestMethod.POST)
	public @ResponseBody ApiMarvelResponse<LoadResponse> loadCharacter(@RequestParam(required = true) String name) {
		return new ApiMarvelResponse<LoadResponse>(marvelServices.loadCharacter(name));
	}

	@RequestMapping(value = "/characters/", method = RequestMethod.POST)
	public @ResponseBody ApiMarvelResponse<Character> createCharacter(@RequestBody CreateCharacterRequest character) {
		Set<ConstraintViolation<CreateCharacterRequest>> validate = validator.validate(character);
		if (validate != null && !validate.isEmpty()) {
			throw new MissingFieldsException();
		}
		
		return new ApiMarvelResponse<Character>(marvelPool.createCharacter(new Character(character)));
	}

	@RequestMapping(value = "/characters/", method = RequestMethod.GET)
	public @ResponseBody ApiMarvelResponse<List<Character>> getCharacters(
			@RequestParam(required = false) String nome) {
		return new ApiMarvelResponse<List<Character>>(marvelPool.getCharacters(nome));
	}

	@RequestMapping(value = "/characters/{id}", method = RequestMethod.GET)
	public @ResponseBody ApiMarvelResponse<Character> getCharacter(@PathVariable(value = "id") Long id) {
		return new ApiMarvelResponse<Character>(marvelPool.getCharacter(id));
	}

	@RequestMapping(value = "/characters/{id}", method = RequestMethod.PUT)
	public @ResponseBody ApiMarvelResponse<Character> alterCharacter(@PathVariable(value = "id") Long id,
			@RequestBody AlterDescriptionRequest character) {
		Set<ConstraintViolation<AlterDescriptionRequest>> validate = validator.validate(character);
		if (validate != null) {
			throw new MissingFieldsException();
		}
		return new ApiMarvelResponse<Character>(marvelPool.alterCharacter(id, character));
	}

	@RequestMapping(value = "/characters/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ApiMarvelResponse<Character> deleteCharacter(@PathVariable(value = "id") Long id) {
		marvelPool.deleteCharacter(id);
		return new ApiMarvelResponse<Character>();
	}

	@RequestMapping(value = "/characters/{id}/comic", method = RequestMethod.POST)
	public @ResponseBody ApiMarvelResponse<Comic> createComic(@PathVariable(value = "id") Long id,
			@RequestBody CreateComicRequest comic) {
		Set<ConstraintViolation<CreateComicRequest>> validate = validator.validate(comic);
		if (validate != null && !validate.isEmpty()) {
			throw new MissingFieldsException();
		}
		return new ApiMarvelResponse<Comic>(marvelPool.createComic(id, new Comic(comic)));
	}

	@RequestMapping(value = "/characters/{id}/comics", method = RequestMethod.GET)
	public @ResponseBody ApiMarvelResponse<List<Comic>> getComics(@PathVariable(value = "id") Long id,
			@RequestParam(required = false) String title) {
		return new ApiMarvelResponse<List<Comic>>(marvelPool.getComics(id, title));
	}

	@RequestMapping(value = "/characters/{id}/comic/{comicId}", method = RequestMethod.GET)
	public @ResponseBody ApiMarvelResponse<Comic> getComic(@PathVariable(value = "id") Long id,
			@PathVariable(value = "comicId") Long comicId) {
		return new ApiMarvelResponse<Comic>(marvelPool.getComic(id, comicId));
	}

	@RequestMapping(value = "/characters/{id}/comic/{comicId}", method = RequestMethod.PUT)
	public @ResponseBody ApiMarvelResponse<Comic> alterCharacter(@PathVariable(value = "id") Long id,
			@PathVariable(value = "comicId") Long comicId, @RequestBody AlterDescriptionRequest comic) {
		Set<ConstraintViolation<AlterDescriptionRequest>> validate = validator.validate(comic);
		if (validate != null && !validate.isEmpty()) {
			throw new MissingFieldsException();
		}
		return new ApiMarvelResponse<Comic>(marvelPool.alterComic(id, comicId, comic));
	}

	@RequestMapping(value = "/characters/{id}/comic/{comicId}", method = RequestMethod.DELETE)
	public @ResponseBody ApiMarvelResponse<Comic> deleteComic(@PathVariable(value = "id") Long id,
			@PathVariable(value = "comicId") Long comicId) {
		marvelPool.deleteComic(id, comicId);
		return new ApiMarvelResponse<Comic>();
	}

}
