package br.com.treinamento.dojo.entities;

public class ApiMarvelResponse<T> {
	private Integer status;
	private String message;
	private T payload;

	public ApiMarvelResponse(T payload) {
		this.status = 200;
		this.message = "ok";
		this.payload = payload;
	}

	public ApiMarvelResponse() {
		this.status = 200;
		this.message = "ok";
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getPayload() {
		return payload;
	}

	public void setPayload(T payload) {
		this.payload = payload;
	}

}
