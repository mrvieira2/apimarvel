package br.com.treinamento.dojo.entities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.treinamento.dojo.entities.marvel.MarvelCharacter;
import br.com.treinamento.dojo.entities.requests.CreateCharacterRequest;

public class Character {
	private Long id;
	private String name;
	private String description;
	private String created;
	private String loaded;
	private CharacterComicsStatus statusComics;
	private Integer totalComics;
	private Integer loadedComics;
	private List<Comic> comics;

	@JsonIgnore
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public Character(MarvelCharacter character) {
		id = character.getId();
		name = character.getName();
		description = character.getDescription();
		totalComics = character.getComics().getAvailable();
		loadedComics = 0;
		if (totalComics != null && totalComics > 0) {
			statusComics = CharacterComicsStatus.LOADING;
		}
		comics = new ArrayList<Comic>();
		created = format.format(new Date());
	}

	public Character(CreateCharacterRequest character) {
		id = character.getId();
		name = character.getName();
		description = character.getDescription();
		totalComics = 0;
		loadedComics = 0;
		statusComics = CharacterComicsStatus.LOADED;
		comics = new ArrayList<Comic>();
		created = format.format(new Date());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Comic> getComics() {
		return comics;
	}

	public void setComics(List<Comic> comics) {
		this.comics = comics;
	}

	public void loadComic(Comic comic) {
		synchronized (comics) {
			comics.add(comic);
			loadedComics++;
			if (loadedComics >= totalComics) {
				statusComics = CharacterComicsStatus.LOADED;
				setLoaded(format.format(new Date()));
			}
		}
	}

	public void addComic(Comic comic) {
		comics.add(comic);
		totalComics++;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public CharacterComicsStatus getStatusComics() {
		return statusComics;
	}

	public void setStatusComics(CharacterComicsStatus statusComics) {
		this.statusComics = statusComics;
	}

	public Integer getTotalComics() {
		return totalComics;
	}

	public void setTotalComics(Integer totalComics) {
		this.totalComics = totalComics;
	}

	public Integer getLoadedComics() {
		return loadedComics;
	}

	public void setLoadedComics(Integer loadedComics) {
		this.loadedComics = loadedComics;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getLoaded() {
		return loaded;
	}

	public void setLoaded(String loaded) {
		this.loaded = loaded;
	}

	public List<Comic> getComics(String title) {
		if (title == null || title.equals("")) {
			return comics;
		}
		List<Comic> response = new ArrayList<Comic>();
		for (Comic comic : comics) {
			if (comic.getTitle().toLowerCase().contains(title.toLowerCase())) {
				response.add(comic);
			}
		}
		return response;
	}

	public Comic getComic(Long comicId) {
		for (Comic comic : comics) {
			if (comic.getId().equals(comicId)) {
				return comic;
			}
		}
		return null;
	}

	public boolean deleteComic(Long comicId) {
		Comic pivot = null;
		for (Comic comic : comics) {
			if (comic.getId().equals(comicId)) {
				pivot = comic;
				break;
			}
		}
		if (pivot == null) {
			return false;
		}
		return comics.remove(pivot);
	}

}
