package br.com.treinamento.dojo.entities;

import br.com.treinamento.dojo.entities.marvel.MarvelComic;
import br.com.treinamento.dojo.entities.requests.CreateComicRequest;

public class Comic {
	private Long id;
	private String title;
	private String description;

	public Comic(MarvelComic comic) {
		id = comic.getId();
		title = comic.getTitle();
		description = comic.getDescription();
	}

	public Comic(CreateComicRequest comic) {
		id = comic.getId();
		title = comic.getTitle();
		description = comic.getDescription();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comic other = (Comic) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

}
