package br.com.treinamento.dojo.entities;

import java.util.HashSet;
import java.util.Set;

public class LoadResponse {
	private Integer totalLoaded;
	private Set<Long> loadedIds;

	public LoadResponse() {
		totalLoaded = 0;
		setLoadedIds(new HashSet<Long>());
	}

	public Integer getTotalLoaded() {
		return totalLoaded;
	}

	public void setTotalLoaded(Integer totalLoaded) {
		this.totalLoaded = totalLoaded;
	}

	public void addLoadedId(Long id) {
		if (id != null && !loadedIds.contains(id)) {
			loadedIds.add(id);
			totalLoaded++;
		}
	}

	public Set<Long> getLoadedIds() {
		return loadedIds;
	}

	public void setLoadedIds(Set<Long> loadedIds) {
		this.loadedIds = loadedIds;
	}
}
