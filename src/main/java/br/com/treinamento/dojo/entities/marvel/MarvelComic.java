package br.com.treinamento.dojo.entities.marvel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarvelComic {
	private Long id;
	private String title;
	private String description;
	private Collection characters;

	public Collection getCharacters() {
		return characters;
	}

	public void setCharacters(Collection characters) {
		this.characters = characters;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
