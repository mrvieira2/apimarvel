package br.com.treinamento.dojo.entities.marvel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarvelResponse<T> {
	private Integer code;
	private String status;
	private Data<T> data;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Data<T> getData() {
		return data;
	}

	public void setData(Data<T> data) {
		this.data = data;
	}

	public Integer getSize() {
		if (data != null) {
			return data.getTotal();
		} else {
			return 0;
		}
	}

}
