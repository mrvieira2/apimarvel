package br.com.treinamento.dojo.entities.requests;

public class AlterDescriptionRequest {
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
