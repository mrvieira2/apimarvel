package br.com.treinamento.dojo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.CONFLICT, reason="Character already exists")
public class CharacterAlreadyExistsException extends RuntimeException {

	private static final long serialVersionUID = 5475203737769844396L;

}
