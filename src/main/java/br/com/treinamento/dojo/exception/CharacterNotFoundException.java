package br.com.treinamento.dojo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Character not found")
public class CharacterNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 5475203737769844396L;

}
