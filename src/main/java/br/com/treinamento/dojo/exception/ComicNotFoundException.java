package br.com.treinamento.dojo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Comic not found")
public class ComicNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -8205895772140361746L;

}
