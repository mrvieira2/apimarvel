package br.com.treinamento.dojo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="One or more fields are missing or have wrong types")
public class MissingFieldsException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
}
