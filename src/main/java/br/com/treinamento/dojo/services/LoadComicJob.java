package br.com.treinamento.dojo.services;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.dojo.utils.Constants;

@DisallowConcurrentExecution
public class LoadComicJob implements Job {

	Logger logger = Logger.getLogger("LoadComic");

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap dados = context.getJobDetail().getJobDataMap();
		Long id = dados.getLong(Constants.ID_CHARACTER);
		Integer offset = dados.getInt(Constants.OFFSET);
		
		logger.info(context.getTrigger().getJobKey() + " id: " + id + " - offset:" + offset);

		MarvelServices marvelServices = (MarvelServices) AppConfig.context.getBean("marvelServices");
		marvelServices.loadComics(id, offset);
	}

}
