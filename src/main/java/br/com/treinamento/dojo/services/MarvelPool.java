package br.com.treinamento.dojo.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import br.com.treinamento.dojo.entities.Character;
import br.com.treinamento.dojo.entities.Comic;
import br.com.treinamento.dojo.entities.requests.AlterDescriptionRequest;
import br.com.treinamento.dojo.exception.CharacterAlreadyExistsException;
import br.com.treinamento.dojo.exception.CharacterNotFoundException;
import br.com.treinamento.dojo.exception.ComicNotFoundException;

@Service
public class MarvelPool {
	private Map<Long, Character> characters;

	@PostConstruct
	public void init() {
		characters = new HashMap<Long, Character>();
	}

	public boolean addCharacter(Character character) {
		if (character != null && character.getId() != null && character.getName() != null
				&& !characters.containsKey(character.getId())) {
			characters.put(character.getId(), character);
			return true;
		}
		return false;
	}

	public Character createCharacter(Character character) {

		if (!addCharacter(character)) {
			throw new CharacterAlreadyExistsException();
		}

		return character;
	}

	public List<Character> getCharacters(String name) {
		List<Character> response = new ArrayList<Character>();
		if (name == null) {
			response.addAll(characters.values());
			return response;
		}

		for (Character character : characters.values()) {
			if (character.getName().toLowerCase().contains(name.toLowerCase())) {
				response.add(character);
			}
		}
		return response;
	}

	public void deleteCharacter(Long id) {
		if (!characters.containsKey(id)) {
			throw new CharacterNotFoundException();
		}
		characters.remove(id);
	}

	public Comic createComic(Long id, Comic comic) {
		Character character = getCharacter(id);
		character.addComic(comic);
		return comic;
	}

	public List<Comic> getComics(Long id, String title) {
		Character character = getCharacter(id);
		return character.getComics(title);
	}

	public Comic getComic(Long id, Long comicId) {
		Character character = getCharacter(id);
		Comic comic = character.getComic(comicId);
		if (comic == null) {
			throw new ComicNotFoundException();
		}
		return comic;
	}

	public Comic alterComic(Long id, Long comicId, AlterDescriptionRequest request) {
		Comic comic = getComic(id, comicId);
		comic.setDescription(request.getDescription());
		return comic;
	}

	public void deleteComic(Long id, Long comicId) {
		Character character = getCharacter(id);
		if (!character.deleteComic(comicId)) {
			throw new ComicNotFoundException();
		}
	}

	public Character getCharacter(Long id) {
		Character character = characters.get(id);
		if (character == null) {
			throw new CharacterNotFoundException();
		}
		return character;
	}

	public Character alterCharacter(Long id, AlterDescriptionRequest request) {
		Character character = getCharacter(id);
		character.setDescription(request.getDescription());
		return character;
	}
	
	
}
