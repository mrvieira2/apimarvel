package br.com.treinamento.dojo.services;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.treinamento.dojo.entities.Character;
import br.com.treinamento.dojo.entities.Comic;
import br.com.treinamento.dojo.entities.LoadResponse;
import br.com.treinamento.dojo.entities.marvel.MarvelCharacter;
import br.com.treinamento.dojo.entities.marvel.MarvelComic;
import br.com.treinamento.dojo.entities.marvel.MarvelResponse;
import br.com.treinamento.dojo.utils.Constants;

@Service
public class MarvelServices {

	private static final String BASE_ENDPOINT = "https://gateway.marvel.com:443/v1/public";

	@Autowired
	private MarvelPool marvelPool;

	@Autowired
	private QuartzServices quartzServices;

	Logger logger = Logger.getLogger(MarvelServices.class);

	public LoadResponse loadCharacter(String name) {
		Client client = ClientBuilder.newClient();

		String timestamp = getTimestamp();
		MarvelResponse<MarvelCharacter> response = client.target(BASE_ENDPOINT).path("characters")
				.queryParam("apikey", Constants.APYKEY).queryParam("ts", timestamp)
				.queryParam("hash", getHash(timestamp)).queryParam("nameStartsWith", name).queryParam("orderBy", "name")
				.queryParam("limit", "100").request(MediaType.APPLICATION_JSON)
				.get(new GenericType<MarvelResponse<MarvelCharacter>>() {
				});

		LoadResponse loadResponse = new LoadResponse();
		if (response.getSize() < 0) {
			return loadResponse;
		}

		StringBuilder ids = new StringBuilder("");
		for (MarvelCharacter character : response.getData().getResults()) {
			if (marvelPool.addCharacter(new Character(character))) {
				ids.append(character.getId() + ",");
				loadResponse.addLoadedId(character.getId());
				asyncLoadComics(character.getId(), character.getComics().getAvailable());
			}
		}

		return loadResponse;
	}

	public void loadComics(Long id, int offset) {
		Client client = ClientBuilder.newClient();

		String timestamp;
		timestamp = getTimestamp();
		MarvelResponse<MarvelComic> comicResponse = null;
		try {
			comicResponse = client.target(BASE_ENDPOINT).path("characters/" + id + "/comics")
					.queryParam("apikey", Constants.APYKEY).queryParam("ts", timestamp)
					.queryParam("hash", getHash(timestamp)).queryParam("limit", 100).queryParam("offset", offset)
					.queryParam("orderBy", "title").request(MediaType.APPLICATION_JSON)
					.get(new GenericType<MarvelResponse<MarvelComic>>() {
					});
		} catch (Exception e) {
			schedule(id, offset);
			logger.info("Offset " + offset + " of characther " + id + " not loaded. Scheduled to retry.");
			return;
		}

		for (MarvelComic comic : comicResponse.getData().getResults()) {
			Character loadedCharacter = marvelPool.getCharacter(id);
			loadedCharacter.loadComic(new Comic(comic));

		}

		logger.info("Offset " + offset + " of characther " + id + " successfully processed.");
	}

	public String getHash(String timestamp) {
		String preHash = timestamp + Constants.PRIVATE_KEY + Constants.APYKEY;

		MessageDigest md5 = null;
		byte[] bytes;

		try {
			md5 = MessageDigest.getInstance("MD5");

			bytes = preHash.getBytes("UTF-8");

		} catch (NoSuchAlgorithmException e) {
			return null;
		} catch (UnsupportedEncodingException e) {
			return null;
		}

		byte[] digest = md5.digest(bytes);

		return new BigInteger(1, digest).toString(16);
	}

	public String getTimestamp() {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");

		return format.format(new Date());
	}

	private void asyncLoadComics(Long id, Integer total) {
		Integer pages = (total / 100) + 1;

		for (int i = 0; i < pages; i++) {
			schedule(id, i * 100);
		}
	}

	private void schedule(Long id, Integer offset) {
		quartzServices.scheduleLoadComics(id, offset, getTimestamp());
	}
}
