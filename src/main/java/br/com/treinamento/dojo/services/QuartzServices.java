package br.com.treinamento.dojo.services;

import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ws.rs.InternalServerErrorException;

import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

import br.com.treinamento.dojo.utils.Constants;

@Service
public class QuartzServices {
	@Autowired
	private SchedulerFactoryBean scheduler;

	@PostConstruct
	public void init() {
		Properties quartzProperties = new Properties();
		
		quartzProperties.setProperty("org.quartz.threadPool.threadCount", "10");
		quartzProperties.setProperty("org.quartz.jobStore.misfireThreshold", "6000000");

		scheduler.setQuartzProperties(quartzProperties);
		try {
			scheduler.afterPropertiesSet();
		} catch (Exception e) {
			e.printStackTrace();
		}
		scheduler.start();
	}

	public void scheduleLoadComics(Long id, Integer offset, String timestamp) {
		JobDetail job = JobBuilder.newJob(LoadComicJob.class)
				.withIdentity(id + "-" + offset + "-" + timestamp, "group1").build();

		Trigger trigger = TriggerBuilder.newTrigger().withIdentity(id + "-" + offset + "-" + timestamp, "group1")
				.startNow().build();
		
		JobDataMap parametros = job.getJobDataMap();
		parametros.put(Constants.OFFSET, offset);
		parametros.put(Constants.ID_CHARACTER, id);

		try {
			scheduler.getScheduler().scheduleJob(job, trigger);
		} catch (SchedulerException e) {
			throw new InternalServerErrorException();
		}
	}

}
