package br.com.treinamento.dojo.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class ApiMarvelResponseTest {
	@Test
	public void testConstructor1() {
		ApiMarvelResponse<String> response = new ApiMarvelResponse<String>("teste");
		
		assertEquals(200, response.getStatus().intValue());
		assertEquals("ok", response.getMessage());
		assertEquals("teste", response.getPayload());
	}
	
	@Test
	public void testConstructor2() {
		ApiMarvelResponse<String> response = new ApiMarvelResponse<String>();
		
		assertEquals(200, response.getStatus().intValue());
		assertEquals("ok", response.getMessage());
		assertNull(response.getPayload());
	}
}
