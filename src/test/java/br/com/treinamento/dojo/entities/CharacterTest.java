package br.com.treinamento.dojo.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.com.treinamento.dojo.entities.marvel.Collection;
import br.com.treinamento.dojo.entities.marvel.MarvelCharacter;
import br.com.treinamento.dojo.entities.marvel.MarvelComic;
import br.com.treinamento.dojo.entities.requests.CreateCharacterRequest;

public class CharacterTest {
	@Test
	public void testContructor1() {
		MarvelCharacter response = new MarvelCharacter();
		response.setId(800L);
		response.setName("Super");
		response.setDescription("Super hero");
		
		Collection comics = new Collection();
		comics.setAvailable(50);
		response.setComics(comics);
		
		Character character = new Character(response);
		
		assertEquals(800L, character.getId().longValue());
		assertEquals("Super", character.getName());
		assertEquals("Super hero", character.getDescription());
		assertEquals(50, character.getTotalComics().intValue());
		assertEquals(0, character.getLoadedComics().intValue());
		assertEquals(CharacterComicsStatus.LOADING, character.getStatusComics());
		assertNotNull(character.getComics());
		assertNotNull(character.getCreated());
	}
	
	@Test
	public void testContructor2() {
		CreateCharacterRequest request = new CreateCharacterRequest();
		request.setId(800L);
		request.setName("Super");
		request.setDescription("Super hero");
		
		Character character = new Character(request);
		
		assertEquals(800L, character.getId().longValue());
		assertEquals("Super", character.getName());
		assertEquals("Super hero", character.getDescription());
		assertEquals(0, character.getTotalComics().intValue());
		assertEquals(0, character.getLoadedComics().intValue());
		assertEquals(CharacterComicsStatus.LOADED, character.getStatusComics());
		assertNotNull(character.getComics());
		assertNotNull(character.getCreated());
	}
	
	@Test
	public void testLoadComic1() {
		MarvelCharacter response = new MarvelCharacter();
		
		Collection comics = new Collection();
		comics.setAvailable(1);
		response.setComics(comics);
		
		Character character = new Character(response);
		
		MarvelComic comic = new MarvelComic();
		character.loadComic(new Comic(comic));
		
		assertEquals(1, character.getTotalComics().intValue());
		assertEquals(1, character.getLoadedComics().intValue());
		assertEquals(CharacterComicsStatus.LOADED, character.getStatusComics());
		assertEquals(1, character.getComics().size());
	}
	
	@Test
	public void testLoadComic2() {
		MarvelCharacter response = new MarvelCharacter();
		
		Collection comics = new Collection();
		comics.setAvailable(2);
		response.setComics(comics);
		
		Character character = new Character(response);
		
		MarvelComic comic = new MarvelComic();
		character.loadComic(new Comic(comic));
		
		assertEquals(2, character.getTotalComics().intValue());
		assertEquals(1, character.getLoadedComics().intValue());
		assertEquals(CharacterComicsStatus.LOADING, character.getStatusComics());
		assertEquals(1, character.getComics().size());
	}
	
	@Test
	public void testLoadComic3() {
		MarvelCharacter response = new MarvelCharacter();
		
		Collection comics = new Collection();
		comics.setAvailable(2);
		response.setComics(comics);
		
		Character character = new Character(response);
		
		MarvelComic comic = new MarvelComic();
		character.loadComic(new Comic(comic));
		
		comic = new MarvelComic();
		character.loadComic(new Comic(comic));
		
		assertEquals(2, character.getTotalComics().intValue());
		assertEquals(2, character.getLoadedComics().intValue());
		assertEquals(CharacterComicsStatus.LOADED, character.getStatusComics());
		assertEquals(2, character.getComics().size());
	}
	
	@Test
	public void testAddComic1() {
		CreateCharacterRequest request = new CreateCharacterRequest();
		
		Character character = new Character(request);
		
		MarvelComic comic = new MarvelComic();
		character.addComic(new Comic(comic));
		
		assertEquals(0, character.getLoadedComics().intValue());
		assertEquals(CharacterComicsStatus.LOADED, character.getStatusComics());
		assertEquals(1, character.getComics().size());
	}
	
	@Test
	public void testAddComic2() {
		CreateCharacterRequest request = new CreateCharacterRequest();
		
		Character character = new Character(request);
		
		MarvelComic comic = new MarvelComic();
		character.addComic(new Comic(comic));
		
		comic = new MarvelComic();
		character.addComic(new Comic(comic));
		
		assertEquals(0, character.getLoadedComics().intValue());
		assertEquals(CharacterComicsStatus.LOADED, character.getStatusComics());
		assertEquals(2, character.getComics().size());
	}
	
	@Test
	public void testGetComics() {
		CreateCharacterRequest request = new CreateCharacterRequest();
		
		Character character = new Character(request);
		
		MarvelComic comic = new MarvelComic();
		comic.setTitle("Comic Title");
		character.addComic(new Comic(comic));
		
		comic = new MarvelComic();
		comic.setTitle("book title");
		character.addComic(new Comic(comic));
		
		comic = new MarvelComic();
		comic.setTitle("Jungle Book");
		character.addComic(new Comic(comic));
		
		comic = new MarvelComic();
		comic.setTitle("Spider-man Collection");
		character.addComic(new Comic(comic));
		
		comic = new MarvelComic();
		comic.setTitle("Mighty Thor");
		character.addComic(new Comic(comic));
		
		assertEquals(5, character.getComics().size());
		assertEquals(5, character.getComics(null).size());
		assertEquals(5, character.getComics("").size());
		assertEquals(2, character.getComics("book").size());
		assertEquals(2, character.getComics("BOOK").size());
		assertEquals(1, character.getComics("thor").size());
		assertEquals(0, character.getComics("booklet").size());
		assertEquals(0, character.getComics("HULK").size());
		
	}
	
	@Test
	public void testGetComic() {
		CreateCharacterRequest request = new CreateCharacterRequest();
		
		Character character = new Character(request);
		
		MarvelComic comic = new MarvelComic();
		comic.setId(1L);
		comic.setTitle("Comic Title");
		character.addComic(new Comic(comic));
		
		comic = new MarvelComic();
		comic.setId(2L);
		comic.setTitle("book title");
		character.addComic(new Comic(comic));
		
		comic = new MarvelComic();
		comic.setId(3L);
		comic.setTitle("Jungle Book");
		character.addComic(new Comic(comic));
		
		comic = new MarvelComic();
		comic.setId(4L);
		comic.setTitle("Spider-man Collection");
		character.addComic(new Comic(comic));
		
		comic = new MarvelComic();
		comic.setId(5L);
		comic.setTitle("Mighty Thor");
		character.addComic(new Comic(comic));
		
		assertEquals("Comic Title", character.getComic(1L).getTitle());
		assertEquals("book title", character.getComic(2L).getTitle());
		assertEquals("Jungle Book", character.getComic(3L).getTitle());
		assertEquals("Spider-man Collection", character.getComic(4L).getTitle());
		assertEquals("Mighty Thor", character.getComic(5L).getTitle());
		assertNull(character.getComic(6L));
		assertNull(character.getComic(null));
		
	}
	
	@Test
	public void testDeleteComic() {
		CreateCharacterRequest request = new CreateCharacterRequest();
		
		Character character = new Character(request);
		
		MarvelComic comic = new MarvelComic();
		comic.setId(1L);
		comic.setTitle("Comic Title");
		character.addComic(new Comic(comic));
		
		comic = new MarvelComic();
		comic.setId(2L);
		comic.setTitle("book title");
		character.addComic(new Comic(comic));
		
		comic = new MarvelComic();
		comic.setId(3L);
		comic.setTitle("Jungle Book");
		character.addComic(new Comic(comic));
		
		comic = new MarvelComic();
		comic.setId(4L);
		comic.setTitle("Spider-man Collection");
		character.addComic(new Comic(comic));
		
		comic = new MarvelComic();
		comic.setId(5L);
		comic.setTitle("Mighty Thor");
		character.addComic(new Comic(comic));
		
		assertTrue(character.deleteComic(2L));
		assertTrue(character.deleteComic(5L));
		assertFalse(character.deleteComic(2L));
		assertFalse(character.deleteComic(6L));
		assertEquals(3, character.getComics().size());
		assertEquals("Comic Title", character.getComic(1L).getTitle());
		assertEquals("Jungle Book", character.getComic(3L).getTitle());
		assertEquals("Spider-man Collection", character.getComic(4L).getTitle());
	}
}
