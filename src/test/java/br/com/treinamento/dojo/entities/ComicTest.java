package br.com.treinamento.dojo.entities;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.treinamento.dojo.entities.marvel.MarvelComic;
import br.com.treinamento.dojo.entities.requests.CreateComicRequest;

public class ComicTest {
	@Test
	public void testConstructor1() {
		MarvelComic response = new MarvelComic();
		response.setId(900L);
		response.setDescription("comic book");
		response.setTitle("the amazing hero");
		
		Comic comic = new Comic(response);
		
		assertEquals(900L, comic.getId().longValue());
		assertEquals("comic book", comic.getDescription());
		assertEquals("the amazing hero", comic.getTitle());
	}
	
	@Test
	public void testConstructor2() {
		CreateComicRequest response = new CreateComicRequest();
		response.setId(900L);
		response.setDescription("comic book");
		response.setTitle("the amazing hero");
		
		Comic comic = new Comic(response);
		
		assertEquals(900L, comic.getId().longValue());
		assertEquals("comic book", comic.getDescription());
		assertEquals("the amazing hero", comic.getTitle());
	}
}
