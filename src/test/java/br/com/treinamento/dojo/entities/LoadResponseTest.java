package br.com.treinamento.dojo.entities;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class LoadResponseTest {
	@Test
	public void testConstructor() {
		LoadResponse loadResponse = new LoadResponse();
		
		assertEquals(0, loadResponse.getTotalLoaded().intValue());
		assertNotNull(loadResponse.getLoadedIds());
	}
	
	@Test
	public void testAddLoadedId() {
		LoadResponse loadResponse = new LoadResponse();
		
		loadResponse.addLoadedId(1L);
		loadResponse.addLoadedId(2L);
		loadResponse.addLoadedId(2L);
		loadResponse.addLoadedId(3L);
		
		assertEquals(3, loadResponse.getTotalLoaded().intValue());
		assertEquals(3, loadResponse.getLoadedIds().size());
		assertTrue(loadResponse.getLoadedIds().contains(1L));
		assertTrue(loadResponse.getLoadedIds().contains(2L));
		assertTrue(loadResponse.getLoadedIds().contains(3L));
		
	}
}
