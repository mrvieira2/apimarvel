package br.com.treinamento.dojo.entities.marvel;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.treinamento.dojo.entities.marvel.Data;
import br.com.treinamento.dojo.entities.marvel.MarvelResponse;

public class MarvelResponseTest {
	@Test
	public void testGetSize() {
		MarvelResponse<Integer> response = new MarvelResponse<Integer>();
		
		Data<Integer> data = new Data<Integer>();
		
		data.setTotal(10);
		
		response.setData(data);
		
		assertEquals(10, response.getSize().intValue());
	}
	
	@Test
	public void testGetSizeWithNullData() {
		MarvelResponse<Integer> response = new MarvelResponse<Integer>();
		
		assertEquals(0, response.getSize().intValue());
	}
}
