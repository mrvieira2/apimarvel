package br.com.treinamento.dojo.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import br.com.treinamento.dojo.entities.Character;
import br.com.treinamento.dojo.entities.Comic;
import br.com.treinamento.dojo.entities.marvel.Collection;
import br.com.treinamento.dojo.entities.marvel.MarvelCharacter;
import br.com.treinamento.dojo.entities.marvel.MarvelComic;
import br.com.treinamento.dojo.entities.requests.AlterDescriptionRequest;
import br.com.treinamento.dojo.exception.CharacterAlreadyExistsException;
import br.com.treinamento.dojo.exception.CharacterNotFoundException;
import br.com.treinamento.dojo.exception.ComicNotFoundException;

public class MarvelPoolTest {
	private MarvelPool marvelPool;

	@Before
	public void init() {
		marvelPool = new MarvelPool();
		marvelPool.init();

		MarvelCharacter response = new MarvelCharacter();
		response.setId(100L);
		response.setName("Deadpool");
		response.setDescription("Merc with a mouth");

		Collection comics = new Collection();
		comics.setAvailable(0);
		response.setComics(comics);

		marvelPool.addCharacter(new Character(response));

		response = new MarvelCharacter();
		response.setId(200L);
		response.setName("Cable");
		response.setDescription("Badass from the future");

		comics = new Collection();
		comics.setAvailable(0);
		response.setComics(comics);

		marvelPool.addCharacter(new Character(response));
	}

	@Test
	public void testAddCharacterAlreadyExists() {
		MarvelCharacter response = new MarvelCharacter();
		response.setId(100L);
		response.setName("Deadpool");
		response.setDescription("Merc with a mouth");

		response.setComics(new Collection());

		assertFalse(marvelPool.addCharacter(new Character(response)));
	}

	@Test
	public void testAddCharacterSuccess() {
		MarvelCharacter response = new MarvelCharacter();
		response.setId(300L);
		response.setName("Wolverine");
		response.setDescription("Logan");

		response.setComics(new Collection());

		assertTrue(marvelPool.addCharacter(new Character(response)));
	}

	@Test
	public void testAddCharacterIdNull() {
		MarvelCharacter response = new MarvelCharacter();
		response.setName("Wolverine");
		response.setDescription("Logan");

		response.setComics(new Collection());

		assertFalse(marvelPool.addCharacter(new Character(response)));
	}

	@Test
	public void testAddCharacterNameNull() {
		MarvelCharacter response = new MarvelCharacter();
		response.setId(300L);
		response.setDescription("Logan");

		response.setComics(new Collection());

		assertFalse(marvelPool.addCharacter(new Character(response)));
	}

	@Test
	public void testCreateCharacterSuccess() {
		MarvelCharacter response = new MarvelCharacter();
		response.setId(300L);
		response.setName("Wolverine");
		response.setDescription("Logan");

		response.setComics(new Collection());

		marvelPool.createCharacter(new Character(response));
	}

	@Test(expected = CharacterAlreadyExistsException.class)
	public void testCreateCharacterAlreadyExists() {
		MarvelCharacter response = new MarvelCharacter();
		response.setId(200L);
		response.setName("Cable");
		response.setDescription("Badass from the future");

		response.setComics(new Collection());

		marvelPool.createCharacter(new Character(response));
	}

	@Test
	public void testGetCharactersByName() {
		assertEquals(2, marvelPool.getCharacters(null).size());
		assertEquals(2, marvelPool.getCharacters("").size());
		assertEquals(1, marvelPool.getCharacters("dEAD").size());
		assertEquals(0, marvelPool.getCharacters("Deadwood").size());
		assertEquals(1, marvelPool.getCharacters("ble").size());
		assertEquals(0, marvelPool.getCharacters("wade wilson").size());

	}

	@Test(expected = CharacterNotFoundException.class)
	public void testDeleteNotExistentCharacter() {
		marvelPool.deleteCharacter(300L);
	}

	@Test
	public void testDeleteCharacter() {
		marvelPool.deleteCharacter(200L);

		assertEquals(1, marvelPool.getCharacters(null).size());
	}

	@Test(expected = CharacterNotFoundException.class)
	public void testAddComicNonExistingCharacter() {
		MarvelComic comic = new MarvelComic();
		comic.setId(10L);
		marvelPool.createComic(300L, new Comic(comic));
	}

	@Test
	public void testAddComic() {
		MarvelComic comic = new MarvelComic();
		comic.setId(10L);
		marvelPool.createComic(100L, new Comic(comic));

		assertEquals(1, marvelPool.getCharacter(100L).getComics().size());

		comic = new MarvelComic();
		comic.setId(10L);
		marvelPool.createComic(100L, new Comic(comic));

		assertEquals(2, marvelPool.getCharacter(100L).getComics().size());
	}

	@Test(expected = CharacterNotFoundException.class)
	public void testGetComicsNonExistentCharacter() {
		MarvelComic comic = new MarvelComic();
		comic.setId(10L);
		comic.setTitle("Cable and Deadpool");
		marvelPool.createComic(300L, new Comic(comic));
	}

	@Test
	public void testGetComics() {
		MarvelComic comic = new MarvelComic();
		comic.setId(10L);
		comic.setTitle("Cable and Deadpool");
		marvelPool.createComic(100L, new Comic(comic));

		comic = new MarvelComic();
		comic.setId(10L);
		comic.setTitle("The Uncanny Xman");
		marvelPool.createComic(100L, new Comic(comic));

		comic = new MarvelComic();
		comic.setId(20L);
		comic.setTitle("Ultimate Xman");
		marvelPool.createComic(100L, new Comic(comic));

		assertEquals(1, marvelPool.getComics(100L, "dead").size());
		assertEquals(1, marvelPool.getComics(100L, "can").size());
		assertEquals(2, marvelPool.getComics(100L, "man").size());
		assertEquals(1, marvelPool.getComics(100L, "MATE").size());
		assertEquals(3, marvelPool.getComics(100L, "").size());
		assertEquals(3, marvelPool.getComics(100L, null).size());
	}

	@Test(expected = CharacterNotFoundException.class)
	public void testGetComicNonExistentCharacter() {
		MarvelComic comic = new MarvelComic();
		comic.setId(10L);
		comic.setTitle("Cable and Deadpool");
		marvelPool.createComic(300L, new Comic(comic));
	}

	@Test
	public void testGetComic() {
		MarvelComic comic = new MarvelComic();
		comic.setId(10L);
		comic.setTitle("Cable and Deadpool");
		marvelPool.createComic(100L, new Comic(comic));

		comic = new MarvelComic();
		comic.setId(10L);
		comic.setTitle("The Uncanny Xman");
		marvelPool.createComic(100L, new Comic(comic));

		comic = new MarvelComic();
		comic.setId(20L);
		comic.setTitle("Ultimate Xman");
		marvelPool.createComic(100L, new Comic(comic));

		assertNotNull(marvelPool.getComic(100L, 10L));
		assertNotNull(marvelPool.getComic(100L, 20L));
	}

	@Test(expected = ComicNotFoundException.class)
	public void testGetComicNotFound() {
		assertNotNull(marvelPool.getComic(100L, 10L));
	}

	@Test(expected = CharacterNotFoundException.class)
	public void testAlterComicCharacterNonExistent() {
		MarvelComic comic = new MarvelComic();
		comic.setId(10L);
		comic.setTitle("Cable and Deadpool");
		comic.setDescription("Regular Adventures");
		marvelPool.createComic(100L, new Comic(comic));

		AlterDescriptionRequest request = new AlterDescriptionRequest();
		request.setDescription("Crazy Adventures");
		marvelPool.alterComic(300L, 10L, request);
	}

	@Test(expected = ComicNotFoundException.class)
	public void testAlterComicComicNonExistent() {
		MarvelComic comic = new MarvelComic();
		comic.setId(10L);
		comic.setTitle("Cable and Deadpool");
		comic.setDescription("Regular Adventures");
		marvelPool.createComic(100L, new Comic(comic));

		AlterDescriptionRequest request = new AlterDescriptionRequest();
		request.setDescription("Crazy Adventures");
		marvelPool.alterComic(100L, 20L, request);
	}

	@Test
	public void testAlterComic() {
		MarvelComic comic = new MarvelComic();
		comic.setId(10L);
		comic.setTitle("Cable and Deadpool");
		comic.setDescription("Regular Adventures");
		marvelPool.createComic(100L, new Comic(comic));

		AlterDescriptionRequest request = new AlterDescriptionRequest();
		request.setDescription("Crazy Adventures");
		marvelPool.alterComic(100L, 10L, request);

		assertEquals("Crazy Adventures", marvelPool.getComic(100L, 10L).getDescription());
	}

	@Test(expected = CharacterNotFoundException.class)
	public void testDeleteComicCharacterNotFound() {
		marvelPool.deleteComic(300L, 10L);
	}

	@Test(expected = ComicNotFoundException.class)
	public void testDeleteComicNotFound() {
		marvelPool.deleteComic(100L, 10L);
	}

	@Test
	public void testDeleteComic() {
		MarvelComic comic = new MarvelComic();
		comic.setId(10L);
		comic.setTitle("Cable and Deadpool");
		comic.setDescription("Regular Adventures");
		marvelPool.createComic(100L, new Comic(comic));

		assertEquals(1, marvelPool.getComics(100L, null).size());

		marvelPool.deleteComic(100L, 10L);

		assertEquals(0, marvelPool.getComics(100L, null).size());
	}

	@Test
	public void testGetCharacter() {
		assertEquals("Deadpool", marvelPool.getCharacter(100L).getName());
	}

	@Test(expected = CharacterNotFoundException.class)
	public void testGetCharacterNotFound() {
		marvelPool.getCharacter(300L);
	}

	@Test(expected = CharacterNotFoundException.class)
	public void testAlterCharacterNotFound() {
		AlterDescriptionRequest request = new AlterDescriptionRequest();
		request.setDescription("El Duderino");
		marvelPool.alterCharacter(300L, request);
	}

	@Test
	public void testAlterCharacter() {
		AlterDescriptionRequest request = new AlterDescriptionRequest();
		request.setDescription("El Duderino");
		marvelPool.alterCharacter(100L, request);
		assertEquals("El Duderino", marvelPool.getCharacter(100L).getDescription());
	}
}
