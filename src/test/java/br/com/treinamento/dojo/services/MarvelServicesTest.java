package br.com.treinamento.dojo.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class MarvelServicesTest {

	@Test
	public void testGetTimestamp() {
		MarvelServices marvelServices = new MarvelServices();
		
		assertNotNull(marvelServices.getTimestamp());
	}
	
	@Test
	public void testGetHash() {
		MarvelServices marvelServices = new MarvelServices();
		
		assertEquals("cdf2af7dadd1405176b75b555eaeb213", marvelServices.getHash("20161128100000"));
	}
}
